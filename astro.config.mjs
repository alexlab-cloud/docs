import { defineConfig } from 'astro/config';
import starlight from '@astrojs/starlight';
import tailwind from '@astrojs/tailwind';

// https://astro.build/config
export default defineConfig({
  integrations: [
    starlight({
      title: 'AlexLab Cloud Docs',
      logo: {
        src: './src/assets/alexlab-cloud.png',
      },
      social: {
        gitlab: 'https://gitlab.com/alexlab-cloud',
      },
      favicon: '/favicons/favicon.ico',
      sidebar: [
        {
          label: 'Projects',
          autogenerate: { directory: 'Projects' },
        },
        {
          label: 'Guides',
          autogenerate: { directory: 'Guides' },
        },
        {
          label: 'Reference',
          autogenerate: { directory: 'Reference' },
        },
      ],
      customCss: [
        // Path to your Tailwind base styles:
        './src/tailwind.css',
      ],
    }),
    tailwind({
      // Disable the default base styles:
      applyBaseStyles: false,
    }),
  ],
});
