# AlexLab Cloud Docs

> Documentation and guides for AlexLab Cloud projects

[![Built with Starlight](https://astro.badg.es/v2/built-with-starlight/tiny.svg)](https://starlight.astro.build)

A simple website for AlexLab Cloud's supporting docs and other guides we want to publish.
