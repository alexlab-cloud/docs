import starlightPlugin from '@astrojs/starlight-tailwind';
import type { Config } from 'tailwindcss';

const accent = {
  950: '#0e052c',
  900: '#1b0b57',
  600: '#441bda',
  200: '#b3a1f3',
};
const slate = {
  950: '#020617',
  900: '#0f172a',
  600: '#475569',
  200: '#e2e8f0',
};

const aero = {
  900: '#032730',
  800: '#054d61',
  700: '#087491',
  600: '#0a9ac2',
  500: '#0fc1f2',
  400: '#3dcdf5',
  300: '#6edaf7',
  200: '#9ee6fa',
  100: '#cff3fc',
};

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      colors: {
        // Your preferred accent color. Indigo is closest to Starlight’s defaults.
        accent: aero,
        // Your preferred gray scale. Zinc is closest to Starlight’s defaults.
        gray: slate,
      },
      fontFamily: {
        // Your preferred text font. Starlight uses a system font stack by default.
        sans: ['"Atkinson Hyperlegible"'],
        // Your preferred code font. Starlight uses system monospace fonts by default.
        mono: ['"IBM Plex Mono"'],
      },
    },
  },
  plugins: [starlightPlugin()],
} satisfies Config;
